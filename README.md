
Directory organisation
----------------------

logilab\_sprint : directory contains the sources of the first test written
                 during a sprint with logilab.

fortran\_copied\_db : extend previous example with complex data structure
                    to illustrate technical principle. It also presents
                    the first pitfall by doing copy without wanting it
                    on data which are not explicitely pointers on C side.

fortran\_shared\_db : a modification of previous example to really share
                    memory of every field of the data structure.

The following link has been used to instanciate a numpy array referencing
a fortran allocated array :
https://gist.github.com/1249305/bd8a6922507b7e5da0f1417fdba77d5115dd12d4


Use and tests
-------------

Currently there is a makefile in each directory.
There should be an example in each directory !

Dependencies
------------

A Fortran compiler implementing the iso\_c\_binding standard.
Cython, Python and NumPy. Tested with:
- gfortran 4.8.2
- python 2.7
- numpy 1.8.0
- cython 0.20

To do
-----

- Use cmake in one of the directories
- Try with python3
- Sort how to reference properly the content of "weird" arrays
- Extensively test references count management

