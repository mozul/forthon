
typedef struct
{
  int itype;
  double val;
  char field[30];
} C_param;

typedef struct
{
  char nickname[30];

  double rsingle;
  int isingle;
  
  int itab[2]; //contains sizes of rtab and weird in that order
  double * rtab;

  C_param * weird;

} C_datatype;

void getOne(int id, C_datatype * d);

void displayOne(int id);
void changeOne(int id, char * nn, double rv, int iv);
