
cimport numpy as np
import numpy as np

import cython as ct

from libc.string cimport strcpy
from dtype cimport C_param, C_datatype, getOne, displayOne, changeOne

np.import_array()

cdef class P_datatype(object):

  cdef int id
  cdef C_datatype f_image

  cdef public np.ndarray rtab

  @ct.locals(i=int)
  def __init__(self, i):
    self.id = i
    getOne(i,&self.f_image)
    # convenient to store the numpy array structure around the pointer
    self.rtab = np.PyArray_SimpleNewFromData(1, [self.f_image.itab[0]], np.NPY_DOUBLE, self.f_image.rtab)

    # beurk
    self.f_image.nickname[30] = '\0'
    for i in range(self.f_image.itab[1]):
      self.f_image.weird[i].field[30] = '\0'

  def fortranContent(self):
    displayOne(self.id)

  def fortranChange(self,nn,rv,iv):
    changeOne(self.id, nn, rv, iv)

  def cythonContent(self):
    cdef int i
    print self.f_image.nickname
    print self.f_image.rsingle
    print self.f_image.isingle
    for i in range(self.f_image.itab[0]):
      print self.f_image.rtab[i]
    print self.rtab
    for i in range(self.f_image.itab[1]):
      print self.f_image.weird[i]

  def cythonChange(self,nn,rv,iv):
    #this is not possible because char[30] is a non-lvalue
    #self.f_image.nickname = nn
    strcpy(self.f_image.nickname, nn)
    self.f_image.rsingle  = rv
    self.f_image.isingle  = iv

  def getWeirdCopy(self,i):
    #as usual careful between Fortran/Python numbering
    if 0 < i <= self.f_image.itab[1]:
      return self.f_image.weird[i-1]
    else:
      raise IndexError

