
import os
import numpy

from distutils.core import setup
from distutils.extension import Extension

from Cython.Distutils import build_ext

setup(name='sprint',
      ext_modules=[Extension('dtype',['dtype.pyx'],
                             libraries=['dtype'],
                             library_dirs=[os.getcwd()],
                             include_dirs=[numpy.get_include()])],
      cmdclass={'build_ext':build_ext}
      )
