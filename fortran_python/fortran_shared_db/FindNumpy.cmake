# - Find numpy
# Find the native numpy includes
# This module defines
#  NUMPY_INCLUDE_DIR, where to find numpy/arrayobject.h, etc.
#  NUMPY_FOUND, If false, do not try to use numpy headers.

if( NOT PYTHONINTERP_FOUND )
  find_package(PythonInterp REQUIRED)
endif( NOT PYTHONINTERP_FOUND )

if (NUMPY_INCLUDE_DIR)
  # in cache already
  set (NUMPY_FIND_QUIETLY TRUE)
endif (NUMPY_INCLUDE_DIR)

if(${PYTHON_VERSION_MAJOR} LESS 3)
  EXEC_PROGRAM (${PYTHON_EXECUTABLE}
    ARGS "-c \"import numpy; print numpy.get_include()\""
    OUTPUT_VARIABLE NUMPY_INCLUDE_DIR
    RETURN_VALUE NUMPY_NOT_FOUND)
else(${PYTHON_VERSION_MAJOR} LESS 3)
  EXEC_PROGRAM (${PYTHON_EXECUTABLE}
    ARGS "-c \"import numpy; print(numpy.get_include())\""
    OUTPUT_VARIABLE NUMPY_INCLUDE_DIR
    RETURN_VALUE NUMPY_NOT_FOUND)
endif(${PYTHON_VERSION_MAJOR} LESS 3)

if (NUMPY_INCLUDE_DIR)
  set (NUMPY_FOUND TRUE)
  set (NUMPY_INCLUDE_DIR ${NUMPY_INCLUDE_DIR} CACHE STRING "Numpy include path")
else (NUMPY_INCLUDE_DIR)
  set(NUMPY_FOUND FALSE)
endif (NUMPY_INCLUDE_DIR)

if (NUMPY_FOUND)
  if (NOT NUMPY_FIND_QUIETLY)
    message (STATUS "Numpy headers found")
  endif (NOT NUMPY_FIND_QUIETLY)
else (NUMPY_FOUND)
  if (NUMPY_FIND_REQUIRED)
    message (FATAL_ERROR "Numpy headers missing")
  endif (NUMPY_FIND_REQUIRED)
endif (NUMPY_FOUND)

MARK_AS_ADVANCED (NUMPY_INCLUDE_DIR)

# checking numpy.i file to include...
if(${PYTHON_VERSION_MAJOR} LESS 3)
  exec_program( ${PYTHON_EXECUTABLE}
                ARGS "-c \"import numpy; print numpy.__version__\""
                OUTPUT_VARIABLE NUMPY_VERSION
               )
else(${PYTHON_VERSION_MAJOR} LESS 3)
  exec_program( ${PYTHON_EXECUTABLE}
                ARGS "-c \"import numpy; print(numpy.__version__)\""
                OUTPUT_VARIABLE NUMPY_VERSION
               )
endif(${PYTHON_VERSION_MAJOR} LESS 3)

string(REGEX REPLACE "([0-9])\\.([0-9])(\\.[0-9])?" "\\1\\2\\3"
       NUMPY_VERSION ${NUMPY_VERSION}
      )

if( NUMPY_VERSION GREATER 17)
  set(NUMPY_DOT_I ${CMAKE_SOURCE_DIR}/tools/swig/numpy-1.8.i)
elseif( NUMPY_VERSION EQUAL 17)
  set(NUMPY_DOT_I ${CMAKE_SOURCE_DIR}/tools/swig/numpy-1.7.i)
else( )
  set(NUMPY_DOT_I ${CMAKE_SOURCE_DIR}/tools/swig/numpy-1.6.i)
endif( )

