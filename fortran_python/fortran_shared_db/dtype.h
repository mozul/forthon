
typedef struct
{
  int itype;
  double val;
  char field[30];
} C_param;

typedef struct
{
  char nickname[30];

  double rsingle;
  int isingle;
  
  int itab[2]; //contains sizes of rtab and weird in that order
  double * rtab;

  C_param * weird;

} C_datatype;

C_datatype * addOne(char * nn, double rv, int iv);
void displayOne(C_datatype * data);
void changeOne(C_datatype * data, char * nn, double rv, int iv);
