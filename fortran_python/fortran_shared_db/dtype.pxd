
cdef extern from 'dtype.h':

  ctypedef struct C_param:
    int itype
    double val
    char field[30]

  ctypedef struct C_datatype:
    char nickname[30]
    double rsingle
    int isingle
    int itab[2] #contain sizes of rtab and weird in that order
    double * rtab
    C_param * weird

  C_datatype * addOne(char * nn, double rv, int iv)
  void displayOne(C_datatype * d)
  void changeOne(C_datatype * d, char * nn, double rv, int iv)
