
cimport numpy as np
import numpy as np

import cython as ct

from libc.string cimport strcpy
from dtype cimport C_param, C_datatype, addOne, displayOne, changeOne

np.import_array()

cdef class P_datatype(object):

  cdef int id
  cdef C_datatype * f_image_ptr

  cdef public np.ndarray rtab

  @ct.locals(i=int)
  def __init__(self,nn,rv,iv):
    self.f_image_ptr = addOne(nn, rv, iv)
    self.rtab = np.PyArray_SimpleNewFromData(1, [self.f_image_ptr.itab[0]], np.NPY_DOUBLE, self.f_image_ptr.rtab)

    # beurk
    self.f_image_ptr.nickname[30] = '\0'
    for i in range(self.f_image_ptr.itab[1]):
      self.f_image_ptr.weird[i].field[30] = '\0'

  def fortranContent(self):
    displayOne(self.f_image_ptr)

  def fortranChange(self,nn,rv,iv):
    changeOne(self.f_image_ptr, nn, rv, iv)

  def cythonContent(self):
    print 'f_image_ptr :'
    print self.f_image_ptr.nickname
    print self.f_image_ptr.rsingle
    print self.f_image_ptr.isingle
    for i in range(self.f_image_ptr.itab[0]):
      print self.f_image_ptr.rtab[i]
    for i in range(self.f_image_ptr.itab[1]):
      print self.f_image_ptr.weird[i]

  def cythonChange(self,nn,rv,iv):
    #this is not possible because char[30] is a non-lvalue
    #self.f_image_ptr.nickname = nn
    strcpy(self.f_image_ptr.nickname, nn)
    self.f_image_ptr.rsingle  = rv
    self.f_image_ptr.isingle  = iv

  def getWeirdCopy(self,i):
    #as usual careful between Fortran/Python numbering
    if 0 < i <= self.f_image_ptr.itab[1]:
      return self.f_image_ptr.weird[i-1]
    else:
      raise IndexError

