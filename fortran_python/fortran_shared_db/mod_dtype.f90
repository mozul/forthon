module data_type

  use iso_c_binding

  implicit none

  private
  
  ! an intermediate type
  type, bind(c) :: T_param
      integer(kind=c_int) :: itype
      real(kind=c_double) :: val
      character(kind=c_char,len=30)   :: field      
  end type T_param


  ! the real type to bind
  type, bind(c) :: T_datatype

     ! a string of size 30
     character(kind=c_char,len=30) :: nickname
   
     ! a real alone
     real(kind=c_double) :: rsingle
     ! an integer alone
     integer(kind=c_int) :: isingle
     
     ! a fixed size array :
     ! contains the sizes of rtab and weird arrays in that order
     integer(kind=c_int), dimension(2) :: itab

     ! a dynamic array
     ! doesn't work like this : real(kind=c_double), dimension(:), pointer :: rtab
     type(c_ptr) :: rtab = c_null_ptr

     ! a pointer on an array of an interoperable type
     !type(T_param), dimension(:), pointer :: weird => null()
     type(c_ptr) :: weird = c_null_ptr

 end type T_datatype

 type T_dt_link
   type(t_datatype), pointer :: d => null()
   type(T_dt_link) , pointer :: n => null()
 end type

 type(T_dt_link), pointer :: root => null()
 type(T_dt_link), pointer :: curr => null()
!----------------------------------------------------------------------

 integer :: nb_data = 0

! --------------------------
! subroutines set to private
! --------------------------

! none

! -------------------------
! subroutines set to public
! -------------------------

! wrap API

 public add_one, display_one, change_one

contains

 function add_one(nn,rv,iv) bind(c, name='addOne')
   implicit none
   real(kind=c_double), intent(in), value :: rv
   integer(kind=c_int), intent(in), value :: iv
   type(c_ptr), value :: nn
   type(c_ptr) :: add_one
   !
   real(kind=8) , dimension(:), pointer :: rtab
   type(T_param), dimension(:), pointer :: weird
   character(kind=c_char,len=30), pointer :: nname
   integer(kind=4) :: i
   type(T_dt_link), pointer :: new

   rtab  => null()
   weird => null()
   new   => null()

   call c_f_pointer(cptr=nn,fptr=nname)

   i = 0
   do while( i <30 .and. nname(i+1:i+1) /= c_null_char )
     i = i + 1
   end do

   allocate(new)
   allocate(new%d)

   if( .not. associated(root) ) then
     root => new
     curr => root
     nb_data = 1
   else
     curr%n => new
     curr => new
     nb_data = nb_data + 1
   end if

   new%d%nickname(1:i) = nname(1:i)
   new%d%nickname(i+1:) = ''

   new%d%rsingle  = rv
   new%d%isingle  = iv

   new%d%itab = (/nb_data,nb_data-1/)

   allocate(rtab(nb_data))
   rtab(:) = 0.
   new%d%rtab = c_loc(rtab(1))

   allocate(weird(nb_data-1))
   do i = 1, nb_data-1
     weird(i)%itype = i
     weird(i)%val   = 0.01d0*i
     write(weird(i)%field,'(A,I0,A,I0)')  'field of ',i,' in ',nb_data
   end do
   new%d%weird = c_loc(weird(1))

   add_one = c_loc(new%d)

 end function

 subroutine display_one(dptr) bind(c, name='displayOne')
   implicit none
   type(c_ptr), intent(in), value :: dptr
   !
   integer(kind=4) :: i
   real(kind=8) , dimension(:), pointer :: rtab
   type(T_param), dimension(:), pointer :: weird
   type(T_datatype) , pointer :: d

   call c_f_pointer(dptr,d)
   if( .not. associated(d) ) then
     return
   end if

   print *,'input T_DATA'
   print *,'  nickname : ', d%nickname
   print *,'  rsingle  : ', d%rsingle
   print *,'  isingle  : ', d%isingle
   print *,'  itab     : ', d%itab
     
   call c_f_pointer(d%rtab ,rtab ,(/d%itab(1)/))
   call c_f_pointer(d%weird,weird,(/d%itab(2)/))

   print *,'  rtab     : ', rtab
   do i = 1, d%itab(2)
     print *,'  weird  : ', i
     print *,'  --itype  : ', weird(i)%itype
     print *,'  --val    : ', weird(i)%val
     print *,'  --field  : ', weird(i)%field
   end do

 end subroutine

 subroutine change_one(dptr, nn, rv, iv) bind(c, name='changeOne')
   implicit none
   type(c_ptr), intent(in), value :: dptr
   real(kind=c_double), intent(in), value :: rv
   integer(kind=c_int), intent(in), value :: iv
   type(c_ptr), value :: nn
   !
   character(kind=c_char,len=30), pointer :: nname
   integer(kind=4) :: i
   type(T_datatype) , pointer :: d

   call c_f_pointer(dptr,d)

   call c_f_pointer(cptr=nn,fptr=nname)
   i = 1
   do while( i <30 .and. nname(i:i) /= c_null_char )
     i = i + 1
   end do

   d%nickname(1:i) = nname(1:i)
   d%nickname(i+1:) = ''

   d%rsingle  = rv
   d%isingle  = iv

 end subroutine
     
end module data_type

