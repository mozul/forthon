
import dtype

print('------------------------------------------')
print('initializing array and getting 2nd element')
d0   = dtype.P_datatype('new from p', 1., 1)
dat  = dtype.P_datatype('second from p', 2., 2)
dat.fortranContent()
dat.cythonContent()
print('------------------------------------------')
print('call to fortranChange with parameters \'fortran change\', 13. 6')
dat.fortranChange('fortran change', 13.,6)
print('------------------------------------------')
print('changing content of dat.rtab[0] = -8000.')
dat.rtab[0] = -8000.
dat.cythonContent()
print('------------------------------------------')
print('call to cythonChange with parameters \'cython change\', 144. 12')
dat.cythonChange('cython change', 144., 12)
dat.fortranContent()
print('------------------------------------------')

print('getting a weird strucutre as a dictionnary and printing key:val')
#getting a weird structure seen
#as a dictionary
di2_1 = dat.getWeirdCopy(1)
for k in di2_1.keys():
  print(str(k)+':'+str(di2_1[k]))
print('------------------------------------------')

# to check handling of null pointers
# within the data structures
print('checking handling of null pointers')
d0.fortranContent()
d0.cythonContent()
