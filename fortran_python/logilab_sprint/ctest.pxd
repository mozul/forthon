
cdef extern from 'type.h':

  ctypedef enum param_names:
    p1, p2, p3, p4

  ctypedef struct myctype:
    int i, j
    double * mat
    double params[4]

  void print_type(myctype * ct)

  void increment(myctype * ct)

