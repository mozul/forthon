
module test_type

  use iso_c_binding

  implicit none

  enum, bind(c)
    enumerator :: p1=1, p2, p3, p4
  end enum

  type, bind(c) :: myftype
    integer(c_int) :: i, j
    type(c_ptr) :: my_mat
    real(c_double), dimension(4) :: params
  end type myftype

  contains

  subroutine print_type(ft) bind(c, name='print_type')
    type(myftype) :: ft
    !
    integer(c_int) :: i,j
    real(c_double), dimension(:,:), pointer :: tab

    call c_f_pointer(fptr=tab,cptr=ft%my_mat,shape=(/ft%j,ft%i/))

    print *,'my mat :'
    print *, 'i :', ft%i
    print *, 'j :', ft%j
    print *, 'tab :'
    do i = 1, ft%i
      do j = 1, ft%j
        write( *, '(f16.14, A)', advance = 'no' ), tab(j,i), ' '
      end do
      print *, ''
    end do
    print *,'param vector :', ft%params
    print *,''

  end subroutine

  subroutine increment(ft) bind(c,name='increment')
    implicit none
    type(myftype) :: ft
    !
    integer(c_int) :: i,j
    real(c_double), dimension(:,:), pointer :: tab

    call c_f_pointer(fptr=tab,cptr=ft%my_mat,shape=(/ft%j,ft%i/))

    do i = 1, ft%i
      do j = 1, ft%j
        tab(j,i) = 0.1d0*i*j
      end do
    end do

  end subroutine

end module test_type
  
