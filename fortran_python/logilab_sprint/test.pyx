
import math
cimport numpy
import numpy

import cython
from ctest cimport p1, p2, myctype, increment, print_type

numpy.import_array()

p = [p1, p2]

cdef class pytype(object):

  #x = cython.declare(myctype)
  cdef myctype x

  cdef public numpy.ndarray p_params

  @cython.locals(a=numpy.ndarray)
  def __init__(self,a):

    a = numpy.asfarray(a)
    self.x = {'i':a.shape[0],'j':a.shape[1],'mat':<double *>a.data}

    cdef numpy.npy_intp s[1]
    s[0] = <numpy.npy_intp> 4
    self.p_params = numpy.PyArray_SimpleNewFromData(1, s, numpy.NPY_DOUBLE, cython.address(self.x).params)
    self.p_params[0:4] = 0.
  
  def dist(self):
    return math.sqrt(self.x.i**2 + self.x.j**2)

  def incr(self):
    increment(cython.address(self.x))
    self.p_params[0:4] += 1.

  def prin(self):
    print_type(cython.address(self.x))

#  @cython.returns(cython.struc)
#  def get_x(self):
#    return self.x
