
typedef enum {
  p1, p2, p3, p4
} param_names;

typedef struct {
    int i, j;
    double *mat;
    double params[4];
} myctype;

//void init_type(int n, int i, int j);

void print_type(myctype * ct);
void increment(myctype * ct);
