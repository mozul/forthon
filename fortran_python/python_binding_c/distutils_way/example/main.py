#!/usr/bin/env python
# -*- coding: utf-8 -*-

def usage( prog_name ):
    print "  Usage:", prog_name
    print "    Example:", prog_name

if __name__ == "__main__":
    import sys

    # Add the local installation directory of the demonstration spam module
    lib_dir  = sys.path[0] + '/../local/lib/python2.7/site-packages/'
    sys.path = [lib_dir] + sys.path

    try:
        import spam
    except ImportError:
        print "  FAIL: Cannot load the demonstration module spam."
        print "    hint: cd ../src ; make ; cd -"
        exit( -1 )


    spam.system('echo "  SUCCESS: Loading spam module done."')

    # if ( len( sys.argv[ 1: ] ) != 2 ):
    #     usage( sys.argv[ 0 ] )
    #     exit( 0 )

