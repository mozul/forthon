from distutils.core import setup, Extension

module1 = Extension('spam',
                    sources = ['spammodule.c'])

setup (name = 'Spam',
       description = 'Tiny example of customized extension python (written in C).',
       version = '1.0',
       author = 'Rozar Fabien',
       author_email = 'fabien.rozar@umontpellier.fr',
       url = 'https://docs.python.org/extending/building',
       long_description = '''
This is really just a demo package.''',
       license = 'LGPLv3',
       platforms = 'any',
       ext_modules = [Extension ('spam', ['spammodule.c'])] )
