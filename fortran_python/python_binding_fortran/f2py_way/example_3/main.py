#!/usr/bin/env python
# -*- coding: utf-8 -*-

def usage( prog_name ):
    print "  Usage:", prog_name
    print "    Example:", prog_name

if __name__ == "__main__":
    import sys

    # Add the local installation directory of the demonstration spam module
    lib_dir  = sys.path[0] + '/../src_3'
    sys.path = [lib_dir] + sys.path

    try:
        import filter
    except ImportError:
        print "  FAIL: Cannot load the demonstration module spam."
        print "    hint: cd ../src_1 ; make ; cd -"
        exit( -1 )

    print filter.dfilter2d.__doc__

    # print add.zadd( [1,2], [1,2] )

    # spam.system('echo "  SUCCESS: Loading spam module done."')

    # if ( len( sys.argv[ 1: ] ) != 2 ):
    #     usage( sys.argv[ 0 ] )
    #     exit( 0 )

