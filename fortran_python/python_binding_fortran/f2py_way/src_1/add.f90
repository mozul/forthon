
subroutine zadd( a, b, c, n )
  double complex a( * )
  double complex b( * )
  double complex c( * )
  integer n

  do j = 1, n
     c( j ) = a( j ) + b( j )
  end do

end subroutine zadd

! f2py -c -m add add.f90
