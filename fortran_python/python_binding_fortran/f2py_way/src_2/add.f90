
subroutine zadd( a, b, c, n )
  implicit none

!F2PY INTENT(OUT) :: C
!F2PY INTENT(HIDE) :: N
!F2PY DOUBLE COMPLEX :: A(N)
!F2PY DOUBLE COMPLEX :: B(N)
!F2PY DOUBLE COMPLEX :: C(N)

  double complex a( * )
  double complex b( * )
  double complex c( * )
  integer n, j

  do j = 1, n
     c( j ) = a( j ) + b( j )
  end do

end subroutine zadd

! f2py -c -m add add.f90
