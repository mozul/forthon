
subroutine dfilter2d( a, b, m, n )
  implicit none

  double precision a( m, n )
  double precision b( m, n )
  integer m, n

!F2PY INTENT(OUT) :: b
!F2PY INTENT(HIDE) :: M
!F2PY INTENT(HIDE) :: N

  integer i, j

  do i = 2, m-1
     do j = 2, n-1
        b( i, j ) = a( i, j ) + &
             ( a( i-1, j   ) + a( i+1, j   ) + &
               a( i  , j-1 ) + a( i  , j+1 ) ) * 0.5D0 + &
             ( a( i-1, j-1 ) + a( i-1, j+1 ) + &
               a( i+1, j-1 ) + a( i+1, j+1 ) ) * 0.25D0               
     end do
  end do

end subroutine dfilter2d

! f2py -c -m filter filter.f90
