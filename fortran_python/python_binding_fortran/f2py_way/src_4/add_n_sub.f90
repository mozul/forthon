
subroutine zadd_n_sub( a, b, c, d, n )
  implicit none

!F2PY INTENT(OUT) :: c
!F2PY INTENT(OUT) :: d
!F2PY INTENT(HIDE) :: N
!F2PY DOUBLE COMPLEX :: a(N)
!F2PY DOUBLE COMPLEX :: b(N)
!F2PY DOUBLE COMPLEX :: c(N)
!F2PY DOUBLE COMPLEX :: d(N)

  double complex a( * )
  double complex b( * )
  double complex c( * )
  double complex d( * )
  integer n, j

  do j = 1, n
     c( j ) = a( j ) + b( j )
     d( j ) = a( j ) - b( j )
  end do

end subroutine zadd_n_sub

! f2py -c -m add_n_sub add_n_sub.f90
