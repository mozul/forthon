
module dataStructure

  use LinearAlgebra, only : sparse_mat

  implicit none

  type, public :: dstruc
    logical :: sym = .false.
    integer(kind=4) :: nb_dofs, nb_nz
    integer(kind=4), dimension(:), pointer :: i_ind => null()
    integer(kind=4), dimension(:), pointer :: j_ind => null()
    real(kind=8)   , dimension(:), pointer :: val => null()
    real(kind=8)   , dimension(:), pointer :: rhs => null()

    type(sparse_mat) :: sparse_sys

  end type

contains

  subroutine systems_generation(systems)
    implicit none
    type(dstruc), dimension(:) :: systems
    !
    integer(kind=4) :: i_sys

    do i_sys = 1, size(systems)

      systems(i_sys)%nb_dofs = 5
      systems(i_sys)%nb_nz   = 12

      allocate(systems(i_sys)%i_ind(systems(i_sys)%nb_nz))
      allocate(systems(i_sys)%j_ind(systems(i_sys)%nb_nz))

      allocate(systems(i_sys)%val(systems(i_sys)%nb_nz))
      allocate(systems(i_sys)%rhs(systems(i_sys)%nb_dofs))

      systems(i_sys)%i_ind = (/  1,   1,   1,   2,    2,   2,    3,   3,   3,   4,   5,   5/)
      systems(i_sys)%j_ind = (/  1,   2,   3,   1,    3,   5,    2,   3,   4,   3,   2,   5/)
      systems(i_sys)%val   = (/2.0, 3.0, 4.0, 3.0, -3.0, 6.0, -1.0, 1.0, 2.0, 2.0, 4.0, 1.0/)
      systems(i_sys)%rhs   = (/20.0, 24.0, 9.0, 6.0,13.0 /)

    end do

  end subroutine

end module

