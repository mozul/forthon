
module LinearAlgebra

  implicit none 

  private

  include 'dmumps_struc.h'

  type, public :: sparse_mat
    private
    type(dmumps_struc) :: mumps_par    
  end type sparse_mat

  public declare, build, &
         solve,   erase, &
         init, finalize

  contains

  subroutine init()
    implicit none

    print *, 'empty initialization'

  end subroutine

  subroutine declare(matrix, nb_dofs, nb_non_zero, i_indices, j_indices, is_sym, info)
    implicit none

    type(sparse_mat) :: matrix

    integer(kind=4)                        :: nb_dofs, nb_non_zero, info
    integer(kind=4), dimension(:), pointer :: i_indices
    integer(kind=4), dimension(:), pointer :: j_indices

    logical                                :: is_sym

    ! initializing a mumps instance
    matrix%mumps_par%job = -1

    matrix%mumps_par%comm = 0

    ! is input symetric ?
    if (is_sym) then
      matrix%mumps_par%sym  = 1
    else
      matrix%mumps_par%sym  = 0
    endif
    ! is parallelism used ?
    matrix%mumps_par%par  = 1

    ! let's do it

    call DMUMPS( matrix%mumps_par )

    ! Analysis
    matrix%mumps_par%job = 1

    ! setting verbosity to minimum
    matrix%mumps_par%ICNTL(1)=0
    matrix%mumps_par%ICNTL(2)=0
    matrix%mumps_par%ICNTL(3)=0
    matrix%mumps_par%ICNTL(4)=0


    ! assembled format input matrix
    matrix%mumps_par%ICNTL(5 ) = 0
    ! triplet coordinates
    matrix%mumps_par%ICNTL(18) = 0
    matrix%mumps_par%N   =  nb_dofs
    matrix%mumps_par%NZ  =  nb_non_zero
    matrix%mumps_par%IRN => i_indices
    matrix%mumps_par%JCN => j_indices

    ! scaling method
    !matrix%mumps_par%ICNTL(8) = 8

    ! let's do it
    call DMUMPS( matrix%mumps_par )

    info = matrix%mumps_par%INFOG(1)

  end subroutine


  subroutine build(matrix, val, info)
    implicit none
    type(sparse_mat) :: matrix
    real(kind=8)   , dimension(:), pointer :: val
    integer(kind=4) :: info

    ! Factorize
    matrix%mumps_par%job = 2

    matrix%mumps_par%A   => val

    ! let's do it
    call DMUMPS( matrix%mumps_par )

    info = matrix%mumps_par%INFOG(1)

  end subroutine

  subroutine solve(matrix, rhs, info)
    implicit none

    type(sparse_mat) :: matrix

    real(kind=8)   , dimension(:), pointer :: rhs
    integer(kind=4) :: info

    ! solve
    matrix%mumps_par%job = 3

    ! rhs
    matrix%mumps_par%RHS => rhs

    ! let's do it
    call DMUMPS( matrix%mumps_par )
    info = -matrix%mumps_par%INFOG(1)

  end subroutine

  subroutine erase(matrix)
    implicit none
    type(sparse_mat) :: matrix

    matrix%mumps_par%job = -2

    ! let's do it
    call DMUMPS( matrix%mumps_par )

  end subroutine

  subroutine finalize()
    implicit none

    print *, 'empty finalization'

  end subroutine

end module
