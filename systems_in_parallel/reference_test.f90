
program test_systems 

  use dataStructure, only : dstruc, systems_generation
  use LinearAlgebra, only : init, declare, build, solve, erase, finalize

  implicit none

  ! number of systems
  integer(kind=4), parameter :: nb_systems = 16
  ! list of sparse systems
  type(dstruc), dimension(nb_systems) :: systems
  !
  integer(kind=4) :: i_sys, info

  call init()

  call systems_generation(systems)

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i_sys)
  !$OMP DO SCHEDULE(RUNTIME)
  do i_sys = 1, nb_systems
    call declare( systems(i_sys)%sparse_sys, systems(i_sys)%nb_dofs, &
                  systems(i_sys)%nb_nz, systems(i_sys)%i_ind       , &
                  systems(i_sys)%j_ind, systems(i_sys)%sym, info     )
    if( info /= 0 ) then
      print *, 'Error when declaring system : ', i_sys
      stop 1
    end if
  end do
  !$OMP END DO
  !$OMP END PARALLEL
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i_sys)
  !$OMP DO SCHEDULE(RUNTIME)
  do i_sys = 1, nb_systems
    call build( systems(i_sys)%sparse_sys, systems(i_sys)%val, info)
    if( info /= 0 ) then
      print *, 'Error when building system : ', i_sys
      stop 2
    end if
  end do
  !$OMP END DO
  !$OMP END PARALLEL
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i_sys)
  !$OMP DO SCHEDULE(RUNTIME)
  do i_sys = 1, nb_systems
    call solve( systems(i_sys)%sparse_sys, systems(i_sys)%rhs, info)
    print *, i_sys, systems(i_sys)%rhs
    if( info /= 0 ) then
      print *, 'Error when solving system : ', i_sys
      stop 3
    end if
  end do
  !$OMP END DO
  !$OMP END PARALLEL
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i_sys)
  !$OMP DO SCHEDULE(RUNTIME)
  do i_sys = 1, nb_systems
    deallocate(systems(i_sys)%i_ind)
    deallocate(systems(i_sys)%j_ind)
    deallocate(systems(i_sys)%val)
    deallocate(systems(i_sys)%rhs)
    call erase( systems(i_sys)%sparse_sys)
  end do
  !$OMP END DO
  !$OMP END PARALLEL
  
  call finalize()

end program
