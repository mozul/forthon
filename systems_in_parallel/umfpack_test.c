
#include "umfpack.h"
#include "stdio.h"

int n=5;
int Ap[]={0,3,6,9,10,12} ;
int Ai[]={0, 1, 2, 0, 2, 4, 1, 2, 3, 2, 1, 4};
double Ax [ ] = {2., 3., 4., 3.,-3., 6.,-1., 1., 2., 2., 4., 1.};
double b [ ] = {20., 24., 9., 6., 13.} ;
double x [5] ;

int main (void)
{
    double *null = (double *) NULL ;
    int i ;
    void *Symbolic, *Numeric ;
    i = umfpack_di_symbolic (n, n, Ap, Ai, null, &Symbolic, null, null) ;
    printf("result of symbolic %d\n",i);
    (void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, null) ;
    umfpack_di_free_symbolic (&Symbolic) ;
    (void) umfpack_di_solve (UMFPACK_Aat, Ap, Ai, Ax, x, b, Numeric, null, null) ;
    umfpack_di_free_numeric (&Numeric) ;
    for (i = 0 ; i < n ; i++) printf ("x [%d] = %g\n", i, x [i]) ;
    return (0) ;
}

